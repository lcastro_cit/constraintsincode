//
//  ViewController.swift
//  dojoConstraintsInCode
//
//  Created by Luciano de Castro Martins on 20/04/2018.
//  Copyright © 2018 luciano. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let blackView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let blueView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let platinView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let scrollView: UIScrollView = {
        let bounds = UIScreen.main.bounds
        let sv = UIScrollView(frame: CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height))
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    let blackCardImageView: UIImageView = {
        let iv = UIImageView(image: #imageLiteral(resourceName: "black_card"))
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.clipsToBounds = true
        return iv
    }()
    
    
    let blueCardImageView: UIImageView = {
        let iv = UIImageView(image: #imageLiteral(resourceName: "blue_card"))
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.clipsToBounds = true
        return iv
    }()
    
    
    let platinCardImageView: UIImageView = {
        let iv = UIImageView(image: #imageLiteral(resourceName: "platin_card"))
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.clipsToBounds = true
        return iv
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addScrollView()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "3 views", style: .plain, target: self, action: #selector(goNext))
    }
    
    @objc func goNext() {
        let nextController = SquaresViewController()
        self.navigationController?.pushViewController(nextController, animated: true)
    }
    func addScrollView() {
        view.addSubview(scrollView)
        
        scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 2).isActive = true
        scrollView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        scrollView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        
        scrollView.addSubview(blackView)
        scrollView.addSubview(blueView)
        scrollView.addSubview(platinView)
        let heightForView: CGFloat = 300
    
        blackView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 20).isActive = true
        blackView.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 5).isActive = true
        blackView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -5).isActive = true
        blackView.heightAnchor.constraint(equalToConstant: heightForView).isActive = true
        blackView.widthAnchor.constraint(lessThanOrEqualToConstant: scrollView.bounds.width).isActive = true
        
        blackView.addSubview(blackCardImageView)
        blackCardImageView.topAnchor.constraint(equalTo: blackView.topAnchor, constant: 0).isActive = true
        blackCardImageView.leftAnchor.constraint(equalTo: blackView.leftAnchor, constant: 0).isActive = true
        blackCardImageView.bottomAnchor.constraint(equalTo: blackView.bottomAnchor, constant: 0).isActive = true
        blackCardImageView.rightAnchor.constraint(equalTo: blackView.rightAnchor, constant: 0).isActive = true
        
        
        blueView.topAnchor.constraint(equalTo: blackView.bottomAnchor, constant: 50).isActive = true
        blueView.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 5).isActive = true
        blueView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -5).isActive = true
        blueView.heightAnchor.constraint(equalToConstant: heightForView).isActive = true
        blueView.widthAnchor.constraint(lessThanOrEqualToConstant: scrollView.bounds.width).isActive = true
        
        blueView.addSubview(blueCardImageView)
        blueCardImageView.topAnchor.constraint(equalTo: blueView.topAnchor, constant: 0).isActive = true
        blueCardImageView.leftAnchor.constraint(equalTo: blueView.leftAnchor, constant: 0).isActive = true
        blueCardImageView.bottomAnchor.constraint(equalTo: blueView.bottomAnchor, constant: 0).isActive = true
        blueCardImageView.rightAnchor.constraint(equalTo: blueView.rightAnchor, constant: 0).isActive = true
        
        
        platinView.topAnchor.constraint(equalTo: blueView.bottomAnchor, constant: 50).isActive = true
        platinView.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 5).isActive = true
        platinView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -5).isActive = true
        platinView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -20).isActive = true
        platinView.heightAnchor.constraint(equalToConstant: heightForView).isActive = true
        platinView.widthAnchor.constraint(lessThanOrEqualToConstant: scrollView.bounds.width).isActive = true
        
        platinView.addSubview(platinCardImageView)
        platinCardImageView.topAnchor.constraint(equalTo: platinView.topAnchor, constant: 0).isActive = true
        platinCardImageView.leftAnchor.constraint(equalTo: platinView.leftAnchor, constant: 0).isActive = true
        platinCardImageView.bottomAnchor.constraint(equalTo: platinView.bottomAnchor, constant: 0).isActive = true
        platinCardImageView.rightAnchor.constraint(equalTo: platinView.rightAnchor, constant: 0).isActive = true
        
        scrollView.contentSize = CGSize(width: 0, height: 1020)
    }
    
    
    
}


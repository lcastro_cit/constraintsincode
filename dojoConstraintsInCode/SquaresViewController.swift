//
//  SquaresViewController.swift
//  dojoConstraintsInCode
//
//  Created by Luciano de Castro Martins on 20/04/2018.
//  Copyright © 2018 luciano. All rights reserved.
//

import UIKit


class SquaresViewController: UIViewController {
    
    static let viewHeight: CGFloat = {
        return UIScreen.main.bounds.height / 3 - 9
    }()
    
    let redView: UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width:  UIScreen.main.bounds.width, height:  viewHeight))
        view.backgroundColor = .red
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let blueView: UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width:  UIScreen.main.bounds.width, height:  viewHeight))
        view.backgroundColor = .blue
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    let orangeView: UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width:  UIScreen.main.bounds.width, height:  viewHeight))
        view.backgroundColor = .orange
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        let stackView = UIStackView(arrangedSubviews: [
            redView, blueView, orangeView])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.spacing = 3
        
        view.addSubview(stackView)
        stackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        stackView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        stackView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        stackView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        
    }
    
}
